;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname TIF__07019D59 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_1
Function Fragment_1(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
PlayerREF.AddItem(SBbathAccessKey,1)
PlayerREF.removeItem(Gold001,(SBbathAccessCost.getvalue() as Int))
PlayerREF.AddSpell(SBBathAcessGrantedSpell,false)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Key property SBbathAccessKey Auto
GlobalVariable Property SBbathAccessCost Auto
MiscObject Property Gold001 Auto
Spell Property SBBathAcessGrantedSpell Auto 

Actor Property PlayerRef Auto